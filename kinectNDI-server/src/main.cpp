#include "ofMain.h"
#include "ofApp.h"

int main() {
    ofSetupOpenGL(1024, 768, OF_WINDOW); // Setup the GL context
    ofRunApp(new ofApp()); // Start the app
}
