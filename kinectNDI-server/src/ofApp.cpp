#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetWindowTitle("kinectNDI-server");
	ofSetFrameRate(60); // run at 60 fps
	ofSetVerticalSync(true);
	KinectNDI.setup();
	gui.setup("KinectNDI");
	gui.add(&KinectNDI.gui);
	gui.loadFromFile("settings.xml");
	KinectNDI._KinectHandler.reinitKinect();
}

//--------------------------------------------------------------
void ofApp::update(){
	KinectNDI.update();

}

//--------------------------------------------------------------
void ofApp::draw(){
	KinectNDI.draw();
	gui.draw();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){
}

void ofApp::exit() {
    KinectNDI._KinectHandler.close(); // Ensure you have a method to close the Kinect in your KinectHandler
}