#pragma once

#include "ofxGui.h"
#include "ofxKinect.h"

class KinectHandler {
public:
    void setup(int kinectIndex = 0);
    void update();
    void draw();
    ofPixels getDepthPixels();
    ofPixels getColorPixels();
    void initGui(ofxGuiGroup* parentGui);
    void reinitKinect();
    void close();
    ofxToggle useInfrared, bEnableDraw, bEnableNearClipWhite;

private:
    ofxKinect kinect;
    ofxButton reinitButton, closeButton;
    ofxFloatSlider minDepthMM, maxDepthMM;
    void setupKinect(bool useInfrared);
    void scaleDepthToMinMax();
    // Update the declaration to accept float parameters
    void onMinDepthChanged(float& value);
    void onMaxDepthChanged(float& value);
    void onUseInfraredChanged(bool& value);
    void onCloseKinectBtn();
    void onEnableNearClipWhiteChanged(bool& value);

};
