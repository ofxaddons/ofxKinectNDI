#pragma once
#include "ofxNDIsender.h"

class NDISender {
public:
    void setup(const std::string& name, int width, int height);
    void send(const ofTexture& texture);
    void send(const ofPixels& pixels);

private:
    ofxNDIsender ndiSender;
    std::string senderName;
};
