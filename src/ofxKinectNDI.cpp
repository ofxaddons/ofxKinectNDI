#include "ofxKinectNDI.h"

void ofxKinectNDI::setup() {
    gui.setup("KinectNDI");
    _KinectHandler.setup(0); // Pass Kinect index if needed
    _KinectHandler.initGui(&gui); // Initialize GUI for KinectHandler
    ndiDepthSender.setup("KinectDepth", 640, 480); // Setup NDISender for depth
    ndiCameraSender.setup("KinectCamera", 640, 480); // Setup NDISender for color
}

void ofxKinectNDI::update() {
    _KinectHandler.update();
}

void ofxKinectNDI::draw() {
    _KinectHandler.draw();
    ndiDepthSender.send(_KinectHandler.getDepthPixels()); // Send depth pixels over NDI
    ndiCameraSender.send(_KinectHandler.getColorPixels()); // Send color pixels over NDI
}
