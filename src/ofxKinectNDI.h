#pragma once

#include "ofMain.h"
#include "KinectHandler.h"
#include "NDISender.h"

class ofxKinectNDI {
public:
    void setup();
    void update();
    void draw();
    ofxGuiGroup gui;
    KinectHandler _KinectHandler;
    NDISender ndiDepthSender;
    NDISender ndiCameraSender;

private:

};
