#include "KinectHandler.h"


void KinectHandler::setup(int kinectIndex) {
    setupKinect(useInfrared);
}

void KinectHandler::close() {
    if(kinect.isConnected()) {
        kinect.close(); // Close the Kinect
    }
}

void KinectHandler::update() {
    if(kinect.isConnected()) {
        kinect.update();
    }
}

void KinectHandler::draw() {
    if(bEnableDraw){
        kinect.drawDepth(0, 0);
        kinect.draw(640, 0); // Draw infrared or RGB based on the mode
    }

}

void KinectHandler::initGui(ofxGuiGroup* parentGui) {
    parentGui->add(useInfrared.setup("Use Infrared", true));
    parentGui->add(reinitButton.setup("Reinitialize"));
    parentGui->add(closeButton.setup("Close"));
    parentGui->add(minDepthMM.setup("Min Depth (mm)", 500, 500, 5000));
    parentGui->add(maxDepthMM.setup("Max Depth (mm)", 5000, 500, 5000));
    parentGui->add(bEnableDraw.setup("EnableDraw",true));
    parentGui->add(bEnableNearClipWhite.setup("EnableNearClipWhite",true));
    
    // Update listeners to match the correct signature
    useInfrared.addListener(this, &KinectHandler::onUseInfraredChanged);
    minDepthMM.addListener(this, &KinectHandler::onMinDepthChanged);
    maxDepthMM.addListener(this, &KinectHandler::onMaxDepthChanged);
    reinitButton.addListener(this, &KinectHandler::reinitKinect);
    closeButton.addListener(this, &KinectHandler::onCloseKinectBtn);
    bEnableNearClipWhite.addListener(this, &KinectHandler::onEnableNearClipWhiteChanged);
}

// Implement the callback methods
void KinectHandler::onUseInfraredChanged(bool& value) {

}

// Implement the callback methods
void KinectHandler::onMinDepthChanged(float& value) {
    // Here, you can handle the change. For now, we'll just call setupKinect.
    if(kinect.isConnected()) {
        kinect.setDepthClipping(minDepthMM, maxDepthMM);
    }
}

void KinectHandler::onMaxDepthChanged(float& value) {
    // Same as above, adjust the Kinect setup or depth clipping
    if(kinect.isConnected()) {
        kinect.setDepthClipping(minDepthMM, maxDepthMM);
    }
}

void KinectHandler::onCloseKinectBtn() {
    close(); // Close the Kinect safely    
}

void KinectHandler::reinitKinect() {
    close(); // Close the Kinect safely    
    setupKinect(useInfrared);
}

void KinectHandler::onEnableNearClipWhiteChanged(bool & value){
    if(kinect.isConnected()) {
        kinect.enableDepthNearValueWhite(value);
    }
}

void KinectHandler::setupKinect(bool useInfrared) {
    if(kinect.isConnected()) {
        kinect.close();
    }

    kinect.setRegistration(true);
    kinect.init(useInfrared);
    kinect.open(-1);
   
}


ofPixels KinectHandler::getDepthPixels() {
    // Your implementation here
    return kinect.getDepthPixels();
}

ofPixels KinectHandler::getColorPixels() {
    // Your implementation here
    if(useInfrared) {
        // If you're in infrared mode, this may need to return infrared pixels or similar
        return kinect.getPixels(); // Adjust according to your actual implementation
    } else {
        return kinect.getPixels();
    }
}
