#include "NDISender.h"

void NDISender::setup(const std::string& name, int width, int height) {
    senderName = name;
    ndiSender.CreateSender(senderName.c_str(), width, height);
}

void NDISender::send(const ofTexture& texture) {
    ndiSender.SendImage(texture);
}

void NDISender::send(const ofPixels& pixels) {
    ndiSender.SendImage(pixels);
}